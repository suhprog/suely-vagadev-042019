import React, {Component} from 'react';

import Header from './components/Header';
import Main from './components/Main';
import Modal from './components/Modal';
import dummyData from './data/data.json';

let productsData = dummyData;

export default class App extends Component {
  constructor(){
    let cartEmpty = ['Seu carrinho está vazio :('];
    super();
    this.state = {
        total: 0,
        modal: false,
        inCart: cartEmpty,
    };
  }
  
  _updateCart(id) {
    
    let cartFooter = '';
    let cartItems = '';

    this.setState({
      total: this.state.total + 1,
      modal: !this.state.modal,
    });

    cartItems = document.getElementById('cart-list').innerHTML + `
      <li class="cart-item" id="` + productsData[parseInt(id.target.id)].id + `"><a href="#"><span class="item-name">` + productsData[parseInt(id.target.id)].name + `</span><span class="item-price">R$ ` + productsData[parseInt(id.target.id)].deal + `</span></a></li>
      <li><hr class='line'></li>
    `;

    cartFooter = `
      <li class="cart-buy"><input type="submit" class="btn-buy" name="submit" value="COMPRAR"></li>
    `;

    document.getElementById('cart-footer').innerHTML = cartFooter;
    document.getElementById('cart-list').innerHTML = cartItems;

  }

  _toggleModal() {
    this.setState({
      modal: !this.state.modal,
    });
  }
 
  render(){
    return (
      <div className="App">
            <Header items={this.state.total} inCart={this.state.inCart}/>
            <Main addIn={this._updateCart.bind(this)} productsData={productsData}/>
            {this.state.modal && (<Modal toggleModal={this._toggleModal.bind(this)} />)}
      </div>
    );
  }
}