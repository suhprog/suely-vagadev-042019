import React from 'react';

import RecProducts from './RecProducts';

export default class Recommended extends React.Component {
    render(){
        return (
            <div className='recommended-products'>
                <div className='description-title'>
                    <h3>Quem viu, viu também</h3>
                </div>
                    <RecProducts addIn={this.props.addIn.bind(this)} productsData={this.props.productsData}/>
            </div>
        );
    }
}