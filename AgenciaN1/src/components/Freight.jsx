import React from 'react';

export default class Freight extends React.Component {
    render(){
        return(
            <div className='mid-freight'>
                <h3 className='freight-text'>CALCULE O FRETE</h3><br />
                <form className='freight'> 
                    <input className='freight-5' type='number' name='freight' placeholder='00000' max='99999' required />
                    <input className='freight-3' type='number' name='freight' placeholder='000' max='999' required />
                    <input className='btn-freight' type='submit' value='Calcular' />
                </form>
            </div>
    );
    }
}