import React from 'react';
import Freight from './Freight';
import ProductThumb from './DesktopInfo';
import ProductImg from './ProductDescription';
import ProductTitle from './ProductTitle'

export default class ProductInfo extends React.Component {
    
    render() {
        return(
            <div id='product-information'>
                <ProductTitle name={this.props.productData.name}/>
                <ProductThumb productData={this.props.productData} addIn={this.props.addIn.bind(this.id)}/>
                <Freight />
                <ProductImg description={this.props.productData.description}/>
            </div>
        );
    }
}

