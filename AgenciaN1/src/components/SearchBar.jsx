import React from 'react';

export default class SearchBar extends React.Component {
    render() {
        return(
                <input id='searchInput' type='text' placeholder='O que deseja buscar?' />
        );
    }
}