import React from 'react';

export default class Modal extends React.Component {
    render(){
        return(
            <div id="modal">
                <div className="modal-background">
                    <div className="modal">
                        <div className="modal-body">
                            <div className="modal-sucess-icon">
                                <img src="img/success.png" alt="success" />
                            </div>
                            <div className="modal-text">PRODUTO ADICIONADO AO CARRINHO</div>
                            <div className="modal-btn">
                                <button className="close-modal-btn" id="close-modal-btn" onClick={this.props.toggleModal.bind(this)}>OK!</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
