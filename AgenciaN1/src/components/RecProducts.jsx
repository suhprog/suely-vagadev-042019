import React from 'react';

import ImgRecommended from './ImgRecommended';

export default class RecProducts extends React.Component {
    render(){

        var myProducts = [];
        for (var i = 1; i < this.props.productsData.length; i++) {
            myProducts.push(
                <div className='recommended-product' key={i}>
                    <div className='recommended-product-img'>
                        <ImgRecommended renderImg={this.props.productsData[i].images}/>
                    </div>
                    <div className='recommended-product-info'>
                        <div className='recommended-product-title'><a href='#'>{this.props.productsData[i].name}</a></div>
                        <div className='full-price'>de <span className='full-number'>R$ {this.props.productsData[i].price}</span></div>
                        <div className='deal-price'>por <span className='deal-number'>R$ {this.props.productsData[i].deal}</span></div>
                        <div className='product-buy'><input type='submit' className='btn-buy' name='submit' value='COMPRA AE' id={this.props.productsData[i].id} onClick={this.props.addIn.bind(this)} /></div>
                    </div>
                </div>
            );
        }
        
        return (
            <div className='recommended-products-list' id='recommended-products-list'>{myProducts}</div>
        );
    }
}