import React from 'react';

export default class ImgRecommended extends React.Component {
    render(){

        return(
            <img src={this.props.renderImg[0].url} alt='' />
        );
    }
}